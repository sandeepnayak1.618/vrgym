from subprocess import Popen
import psutil
import os
import platform
import configparser
import random
import socket
import json
import time


port_list = range(20000, 50000)
proc_dict = {}

bridge_params_dict = {'host_vrenv' : None, \
                      'port_vrenv' : None, \
                      'host_ros' : None, \
                      'port_ros' : None, \
                      'host_ros_recv' : None, \
                      'port_ros_recv' : None, \
                      'env_name' : None, \
                      'task' : None}


def parse_params(config_file):
    cparser = configparser.ConfigParser()
    cparser.read(config_file)

    bridge_params_dict['host_vrenv'] = cparser['VRGYMENV_MANAGER']['HOST']
    bridge_params_dict['port_vrenv'] = int(cparser['VRGYMENV_MANAGER']['PORT'])
    bridge_params_dict['host_ros_recv'] = cparser['ROS_RECEIVER']['HOST']
    bridge_params_dict['port_ros_recv'] = int(cparser['ROS_RECEIVER']['PORT'])
    bridge_params_dict['env_name'] = cparser['VRGYMENV_MANAGER']['ENVNAME']
    bridge_params_dict['task'] = cparser['VRGYMENV_MANAGER']['TASK']


def sample_port():
    sampled = False
    port = -1
    while not sampled:
        port = random.choice(port_list)
        sock_temp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            ipaddr = socket.gethostbyname(socket.gethostname())
            sock_temp.bind((ipaddr, port))
            sampled = True
        except:
            continue
        sock_temp.close()
    
    return port


def gen_platform_cmd():
    platform_name = platform.platform()

    if 'Windows' in platform_name:
        return os.path.abspath(os.path.join('packagedWin', bridge_params_dict['env_name']+'.exe'))
    elif 'Linux' in platform_name:
        return os.path.abspath(os.path.join('packagedLinux', bridge_params_dict['env_name']+'.sh'))
    else:
        raise Exception('platform not support')


def invoke_env(port):
    paltform_cmd = gen_platform_cmd()
    arg_port = ' -task ' + bridge_params_dict['task'] + ' -host ' + bridge_params_dict['host_vrenv'] + ' ' + '-port ' + str(port) + ' ' + \
    '-host_ros_recv ' + bridge_params_dict['host_ros_recv'] + ' ' + '-port_ros_recv ' + str(bridge_params_dict['port_ros_recv'])
    try:
        proc = Popen([paltform_cmd, arg_port], shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)
        proc_dict[port] = proc
    except:
        print('invoke vrgym env failed')
        return False

    return True


def close_env(pid):
    process = psutil.Process(pid)
    for proc in process.children(recursive=True):
        proc.kill()
    process.kill()


def close_env_all():
    for port in proc_dict.keys():
        process = proc_dict[port]
        for proc in process.children(recursive=True):
            proc.kill()
        process.kill()


def comm_env():
    host = socket.gethostbyname(socket.gethostname())
    port_env = bridge_params_dict['port_vrenv']
    sock_env = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock_env.bind((host, port_env))
    except:
        raise Exception('env manager port is occupied, please check/modify config file')
    
    sock_env.listen()
    conn_ros, _ = sock_env.accept()

    with conn_ros:
        print('env manager has established one connection!')
        while True:
            data = conn_ros.recv(2048).decode()
            # if not data:
            #     break
            if data:
                print('manage data coming ...')
                obj = json.loads(data)
                if not 'cmd' in obj.keys():
                    print('invalid incoming data ...')
                    continue

                msg_reply = {}
                cmd = obj['cmd']
                if cmd == 'ENVLAUNCH':
                    port_sampled = sample_port()
                    if port_sampled < 0:
                        raise Exception('no port resource is available')
                    else:
                        if invoke_env(port_sampled):
                            msg_reply['env'] = 'launched'
                            msg_reply['host'] = host
                            msg_reply['port'] = port_sampled
                        else:
                            msg_reply['env'] = 'failed'
                elif cmd == 'ENVCLOSE':
                    if not 'port' in obj.keys():
                        print('invalid key parameter ...')
                        continue
                    else:
                        port = int(obj['port'])
                        close_env(proc_dict[port].pid)
                        msg_reply['close'] = 'closed'
                        msg_reply['host'] = host
                        msg_reply['port'] = port
                
                conn_ros.sendall(json.dumps(msg_reply).encode())


if __name__ == '__main__':
    config_file = os.path.join('bridge_config.ini')
    parse_params(config_file)

    comm_env()