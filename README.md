# VRGym

This project incorporates UE4 virtual environment and modules interacting with the virtual scene.

## Organization

In directory *VRGymEnv* it contains four minimum components to support one UE4 project:

* **Config**: UE4 project basic configuration
* **Content**: UE4 project assets including scenes / models / blueprints
* **Source**: UE4 project source files
* **.uproject**: UE4 project editor entrance file

## Building / Testing Environment

Windows 10 + Unreal Engine 4.20 + Visual Studio 2017 (Version >= 15.6). 
Cliking *.uproject* to start with building.

UE Module of libprotobuf is pre-built under Visual Studio 2015 (Win64) and Clang 5.0.0 (Linux) for VRGym communication usage. Google Protobuf version we adopt is 3.5.1.

## Virtual Environment

Inside UE4 editor, level *GymTestMap* is created as current envrionment map. Unit runtime tests can be conducted inside this map.

## Development

To facilitate development, the MS Visual Studio *.sln* file can be generated by right click *.uproject* file and select *Generate Visual Studio project files*.
Majority of development work can be done in Visual Studio.

## Packaged Environment

Steps to package *VRGymEnv*:

* Select *packaging settings* in UE4Editor
* Select the level name and corresponding game mode are expected to export. In current case, *GymTestMap* is the level name with game mode "GameMode".
* Select *packaging project* and specify the platform as *Win64* and the export path
* Executable with *.exe* can be found in export path. Command Line can be enabled in game by typing *`*