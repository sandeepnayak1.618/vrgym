import os
import torch
import numpy as np


def run_episodes(agent, report_epi=20):
    steps = []
    rewards = []

    epi = 0
    while True:
        reward, step = agent.episode()
        steps.append(step)
        rewards.append(reward)
        if epi % report_epi == 0:
            print('total steps: {0}, avg reward {1}'.format(sum(steps), np.mean(rewards[-100:])))


def run_iterations(agent, report_iter=20):
    steps = []
    rewards = []

    epi = 0
    while True:
        agent.iteration()
        steps.append(agent.total_steps)
        rewards.append(np.mean(agent.last_episode_rewards))
        if epi % report_iter == 0:
            print('total steps: {0}, avg reward {1}'.format(sum(steps), np.mean(rewards[-100:])))


def state_trans_rgb2gray(states_rgb, state_dim):
    assert len(states_rgb) > 0
    assert len(state_dim) == 3
    assert state_dim[0] == 3
    
    states_gray = []
    for state_rgb in states_rgb:
        state = np.frombuffer(state_rgb, dtype=np.uint8)
        assert len(state) == 3*state_dim[1]*state_dim[2]
        state = state.reshape((-1, 3))
        state_gray = .299*state[:, 0]+.587*state[:, 1]+.114*state[:, 2]
        state_gray = state_gray.astype(np.uint8)/255.0
        states_gray.append(state_gray.reshape((1, state_dim[1], state_dim[2])))
    
    return np.stack(states_gray)


class Batcher:
    def __init__(self, batch_size, data):
        self.batch_size = batch_size
        self.data = data
        self.num_entries = len(data[0])
        self.reset()

    def reset(self):
        self.batch_start = 0
        self.batch_end = self.batch_start + self.batch_size

    def end(self):
        return self.batch_start >= self.num_entries

    def next_batch(self):
        batch = []
        for d in self.data:
            batch.append(d[self.batch_start: self.batch_end])
        self.batch_start = self.batch_end
        self.batch_end = min(self.batch_start + self.batch_size, self.num_entries)
        return batch

    def shuffle(self):
        indices = np.arange(self.num_entries)
        np.random.shuffle(indices)
        self.data = [d[indices] for d in self.data]