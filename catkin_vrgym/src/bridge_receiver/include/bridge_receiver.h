#pragma once

#include <ros/ros.h>
#include <std_msgs/String.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>

#include <boost/thread.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <unistd.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

// defination of bridge receiver capacity
#define RECV_BUFFER_SIZE 200000

using namespace rapidjson;

class BridgeReceiver{

public:
    BridgeReceiver();
    ~BridgeReceiver();

    // communication
    void EstablishEnvComm();

private:
    // ros handler
    ros::NodeHandle _nh;
    ros::Publisher _recv_agent_pub;
    ros::Publisher _recv_manager_pub;
    ros::Subscriber _recv_manager_sub;
    void OnRecvManager(const std_msgs::String msg);
    unsigned int MessagePublish2Agent(const unsigned char* Data);
    

    // communication properties
    int _sock_desc;
    int* _sock_clients;
    struct sockaddr_in _server, _client;
    fd_set _readfds;
    int _recv_port;
    bool _ready2connect;
    void BufferedRecv();

    const unsigned int _maxn_client = 16;

    // buffering properties
    unsigned char* _DataReceived;
    unsigned int _BytesReceived;
    unsigned int _BufferSize;
};