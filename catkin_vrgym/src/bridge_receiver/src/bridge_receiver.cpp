#include "bridge_receiver.h"

static std_msgs::String message_wrapper(const char* status)
{
    Document doc;
    Value msg(kObjectType);
    Value v_res;
    v_res.SetString(ros::this_node::getName().c_str(), doc.GetAllocator());
    Value v_status;
    v_status.SetString(status, doc.GetAllocator());

    msg.AddMember("res", v_res, doc.GetAllocator());
    msg.AddMember("status", v_status, doc.GetAllocator());
    StringBuffer msg_buf;
    Writer<StringBuffer> msg_writer(msg_buf);
    msg.Accept(msg_writer);
    std::string msg_str = msg_buf.GetString();

    std_msgs::String msg_manager;
    msg_manager.data = msg_str;

    return msg_manager;
}

BridgeReceiver::BridgeReceiver()
{
    _sock_desc = socket(AF_INET, SOCK_STREAM, 0);
    if(_sock_desc <= 0)
    {
        printf("receiver socket initialization failed, quit ...\n");
        exit(-1);
    }

    _recv_agent_pub = _nh.advertise<std_msgs::String>("/agent_data_res", 1000);
    _recv_manager_pub = _nh.advertise<std_msgs::String>("/receiver_res", 1);
    _recv_manager_sub = _nh.subscribe("/receiver_config", 1, &BridgeReceiver::OnRecvManager, this);
    
    // while(_recv_agent_pub.getNumSubscribers() == 0) continue;
    while(_recv_manager_pub.getNumSubscribers() == 0) continue;
    printf("contact manager success\n");

    _sock_clients = new int[_maxn_client];
    memset(_sock_clients, 0, sizeof(int)*_maxn_client);

    _recv_port = -1;
}

BridgeReceiver::~BridgeReceiver()
{
    delete[] _sock_clients;
    delete[] _DataReceived;
}

void BridgeReceiver::EstablishEnvComm()
{
    while(_recv_port < 0) ros::Duration(.001).sleep();

    _server.sin_addr.s_addr = INADDR_ANY;
    _server.sin_family = AF_INET;
    _server.sin_port = htons(_recv_port);

    if(bind(_sock_desc, (struct sockaddr*)&_server, sizeof(_server)) < 0)
    {
        printf("receiver can not establish socket on certain port, quit ...\n");
        std_msgs::String msg_res = message_wrapper("failed");
        _recv_manager_pub.publish(msg_res);
        exit(-1);
    }

    if(listen(_sock_desc, _maxn_client) < 0)
    {
        printf("receiver meets with exception listening on certain port, quit ...\n");
        std_msgs::String msg_res = message_wrapper("failed");
        _recv_manager_pub.publish(msg_res);
        exit(-1);
    }

    _ready2connect = true;
    _BufferSize = RECV_BUFFER_SIZE;
    _DataReceived = new unsigned char[_BufferSize];

    _BytesReceived = 0;
    if(_ready2connect)
    {
        printf("receiver node starts successfully!\n");
        std_msgs::String msg_res = message_wrapper("success");
        _recv_manager_pub.publish(msg_res);
        BufferedRecv();
    }
}

void BridgeReceiver::OnRecvManager(const std_msgs::String msg)
{
    printf("manager message coming\n");
    Document recv_manager_doc;
    recv_manager_doc.Parse(msg.data.c_str());
    if(recv_manager_doc.HasMember("cmd") && recv_manager_doc.HasMember("port"))
    {
        std::string cmd = recv_manager_doc["cmd"].GetString();
        if(cmd == "init_recv_port")
        {
            _recv_port = recv_manager_doc["port"].GetInt();
            return;
        }
    }

    std_msgs::String msg_res = message_wrapper("failed");
    _recv_manager_pub.publish(msg_res);
}

unsigned int BridgeReceiver::MessagePublish2Agent(const unsigned char* Data)
{
    static unsigned char* buffer = new unsigned char[_BufferSize];

    unsigned int Consumed = 0;
    unsigned int Previous = 0;

    for(int i=0; i<_BytesReceived; i++)
    {
        if(Data[i] == '\n')
        {
            memmove(buffer, Data+Previous, i-Previous);
            buffer[i-Previous] = 0;

            std::string msg_str(reinterpret_cast<char *>(buffer));
            std_msgs::String msg_agent;
            msg_agent.data = msg_str;
            _recv_agent_pub.publish(msg_agent);

            Consumed = i+1;
            Previous = i+1;
        }
    }

    return Consumed;
}

void BridgeReceiver::BufferedRecv()
{   
    int max_sd = 0;
    while(ros::ok())
    {
        FD_ZERO(&_readfds);

        FD_SET(_sock_desc, &_readfds);
        max_sd = _sock_desc;

        for(int i=0; i<_maxn_client; i++)
        {
            if(_sock_clients[i] > 0)
                FD_SET(_sock_clients[i], &_readfds);

            if(_sock_clients[i] > max_sd)
            max_sd = _sock_clients[i];
        }    

        int activity = select(max_sd+1, &_readfds, NULL, NULL, NULL);
        if(activity < 0)
        {
            printf("receiver activity selection failed, quit ...\n");
            exit(-1);
        }

        int addrlen = sizeof(_server);
        int sock_new = 0;
        if(FD_ISSET(_sock_desc, &_readfds))
        {
            if((sock_new = accept(_sock_desc, (struct sockaddr*)&_client, (socklen_t*)&addrlen)) < 0)
            {
                printf("receiver accept connection failed, quit ...\n");
                exit(-1);
            }
            getpeername(sock_new, (struct sockaddr*)&_client, (socklen_t*)&addrlen);
            printf("connected: ip %s, port %d\n", inet_ntoa(_client.sin_addr), ntohs(_client.sin_port));
            for(int i=0; i<_maxn_client; i++)
                if(_sock_clients[i] == 0)
                {
                    _sock_clients[i] = sock_new;
                    break;
                }
        }

        for(int i=0; i<_maxn_client; i++)
        {
            if(FD_ISSET(_sock_clients[i], &_readfds))
            {
                int read_size;
                if((read_size = recv(_sock_clients[i], _DataReceived+_BytesReceived, _BufferSize-_BytesReceived, 0)) < 0)
                {
                    getpeername(_sock_clients[i], (struct sockaddr*)&_client, (socklen_t*)&addrlen);
                    printf("disconnected: ip %s, port %d\n", inet_ntoa(_client.sin_addr), ntohs(_client.sin_port));
                    close(_sock_clients[i]);
                    _sock_clients[i] = 0;
                }
                else if(read_size > 0)
                {
                    _BytesReceived += read_size;
                    unsigned int consumed = MessagePublish2Agent(_DataReceived);
                    if(consumed > 0)
                    {
                        memmove(_DataReceived, _DataReceived+consumed, _BytesReceived-consumed);
                        _BytesReceived -= consumed;
                    }
                }
            }
        }
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "bridge_receiver");
    BridgeReceiver br;
    boost::thread recv_working_thread(boost::bind(&BridgeReceiver::EstablishEnvComm, &br));
    ros::spin();
    recv_working_thread.join();

    return 0;
}