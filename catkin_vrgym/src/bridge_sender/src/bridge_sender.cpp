#include "bridge_sender.h"

static std_msgs::String message_wrapper(const char* status)
{
    Document doc;
    Value msg(kObjectType);
    Value v_res;
    v_res.SetString(ros::this_node::getName().c_str(), doc.GetAllocator());
    Value v_status;
    v_status.SetString(status, doc.GetAllocator());

    msg.AddMember("res", v_res, doc.GetAllocator());
    msg.AddMember("status", v_status, doc.GetAllocator());
    StringBuffer msg_buf;
    Writer<StringBuffer> msg_writer(msg_buf);
    msg.Accept(msg_writer);
    std::string msg_str = msg_buf.GetString();

    std_msgs::String msg_manager;
    msg_manager.data = msg_str;

    return msg_manager;
}

BridgeSender::BridgeSender()
{
    _send_agent_sub = _nh.subscribe("/agent_data_req", 1000, &BridgeSender::OnRecvAgent, this);
    _send_manager_pub = _nh.advertise<std_msgs::String>("/sender_res", 1);
    _send_manager_sub = _nh.subscribe("/sender_config", 1, &BridgeSender::OnRecvManager, this);

    while(_send_manager_pub.getNumSubscribers() == 0) continue;
    printf("contact manager success\n");

    _conn_new = false;
    _conn_close = false;
}

BridgeSender::~BridgeSender()
{}

void BridgeSender::EstablishEnvComm()
{
    DataRecv();
}

void BridgeSender::OnRecvManager(const std_msgs::String msg)
{
    printf("manager message coming\n");
    std_msgs::String msg_res;

    Document send_manager_doc;
    send_manager_doc.Parse(msg.data.c_str());
    if(send_manager_doc.HasMember("cmd") && 
    send_manager_doc.HasMember("host") && 
    send_manager_doc.HasMember("port"))
    {
        std::string cmd = send_manager_doc["cmd"].GetString();
        if(cmd == "invoke_send")
        {
            std::string host = send_manager_doc["host"].GetString();
            int port = send_manager_doc["port"].GetInt();

            _host_config = host;
            _port_config = port;
            _conn_new = true;
            return;
        }
        else if(cmd == "close_send")
        {
            std::string host = send_manager_doc["host"].GetString();
            int port = send_manager_doc["port"].GetInt();

            _host_config = host;
            _port_config = port;
            _conn_close = true;
            return;
        }
    }
    msg_res = message_wrapper("failed");
    _send_manager_pub.publish(msg_res);

    return;
}

void BridgeSender::OnRecvAgent(const std_msgs::String msg)
{
    printf("agent message coming\n");
    Document send_agent_doc;
    send_agent_doc.Parse(msg.data.c_str());
    if(send_agent_doc.HasMember("msg") &&
    send_agent_doc.HasMember("host") &&
    send_agent_doc.HasMember("port") &&
    send_agent_doc.HasMember("node_name") &&
    send_agent_doc.HasMember("task_id"))
    {
        std::string host = send_agent_doc["host"].GetString();
        int port = send_agent_doc["port"].GetInt();
        ADDRESS address_pair = std::make_pair(host, port);
        int sock;
        if((sock = _address2sock[address_pair]) > 0)
        {
            std::string msg_send = msg.data+"\n";
            if(send(sock, msg_send.c_str(), strlen(msg_send.c_str()), 0) < 0)
            {
                printf("destination connection failure!\n");
                _address2sock.erase(address_pair);
            }
            return;
        }
        else printf("destination is not recognized!\n");
    }
    printf("message format is not recognized!\n");
}

void BridgeSender::DataRecv()
{
    while(ros::ok()) continue;
}

bool BridgeSender::_conn_new_handler()
{
    std_msgs::String msg_res;
    ADDRESS address_pair = std::make_pair(_host_config, _port_config);
    if(_address2sock.find(address_pair) == _address2sock.end())
    {
        int sock;
        if((sock = socket(AF_INET, SOCK_STREAM, 0)) <= 0)
        {
            printf("sender socket initialization failed, quit ...\n");
            msg_res = message_wrapper("failed");
            _send_manager_pub.publish(msg_res);
            return false;
        }

        printf("host: %s, port: %d\n", _host_config.c_str(), _port_config);
        _server.sin_addr.s_addr = inet_addr(_host_config.c_str());
        _server.sin_family = AF_INET;
        _server.sin_port = htons(_port_config);

        if(connect(sock, (struct sockaddr*)&_server, sizeof(_server)) < 0)
        {
            printf("sender socket connection failed, quit ...\n");
            msg_res = message_wrapper("failed");
            _send_manager_pub.publish(msg_res);
            return false;
        }
        printf("sender connected\n");

        _address2sock[address_pair] = sock;
        msg_res = message_wrapper("success");
        _send_manager_pub.publish(msg_res);
    }
    else
    {
        printf("sender socket initialization occupied, quit ...\n");
        msg_res = message_wrapper("failed");
        _send_manager_pub.publish(msg_res);
        return false;
    }

    return true;
}   

bool BridgeSender::_conn_close_handler()
{
    std_msgs::String msg_res;
    ADDRESS address_pair = std::make_pair(_host_config, _port_config);
    int sock;
    if((sock = _address2sock[address_pair]) > 0)
    {
        _address2sock.erase(address_pair);
        msg_res = message_wrapper("success");
        _send_manager_pub.publish(msg_res);
    }
    else
    {
        printf("sender close address failed, quit ...\n");
        msg_res = message_wrapper("failed");
        _send_manager_pub.publish(msg_res);
        return false;
    }

    return true;
}

void BridgeSender::ManageConfig()
{
    while(ros::ok())
    {
        if(_conn_new)
        {
            if(!_conn_new_handler())
            {
                printf("sender env new conn failed, quit ...\n");
                exit(-1);
            }
            _conn_new = false;
        }

        if(_conn_close)
        {
            if(!_conn_close_handler())
            {
                printf("sender env close conn failed, quit ...\n");
                exit(-1);
            }
            _conn_close = false;
        }

        ros::spinOnce();
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "bridge_sender");
    BridgeSender bs;
    boost::thread send_working_thread(boost::bind(&BridgeSender::EstablishEnvComm, &bs));
    bs.ManageConfig();
    send_working_thread.join();

    return 0;
}