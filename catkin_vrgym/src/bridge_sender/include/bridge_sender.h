#pragma once

#include <ros/ros.h>
#include <std_msgs/String.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>

#include <boost/thread.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#include <utility>
#include <unordered_map>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

typedef std::pair<std::string, int> ADDRESS;

struct PAIRHASH{
    template <class T1, class T2>
    std::size_t operator() (const std::pair<T1, T2> &p) const{
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);

        return h1 ^ h2;
    }
};

using namespace rapidjson;

class BridgeSender{

public:
    BridgeSender();
    ~BridgeSender();

    // communication
    void EstablishEnvComm();
    void ManageConfig();

private:
    // ros handler
    ros::NodeHandle _nh;
    ros::Subscriber _send_agent_sub;
    ros::Publisher _send_manager_pub;
    ros::Subscriber _send_manager_sub;
    void OnRecvAgent(const std_msgs::String msg);
    void OnRecvManager(const std_msgs::String msg);

    // sender config properties
    std::string _host_config;
    int _port_config;
    bool _conn_new;
    bool _conn_close;
    bool _conn_new_handler();
    bool _conn_close_handler();

    // communication properties
    struct sockaddr_in _server;
    std::unordered_map<ADDRESS, int, PAIRHASH> _address2sock;
    void DataRecv();
};