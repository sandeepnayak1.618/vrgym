#! /usr/bin/env python

import rospy
from std_msgs.msg import String

from threading import Thread
import configparser
import sys
import socket
import json
import time


bridge_params_dict = {  'host_vrenv' : None, 'port_vrenv' : None, \
                        'host_ros_recv' : None, 'port_ros_recv' : None   }


# message sub on both recv and send sides
on_recv = False
on_recv_flag = False
def on_recv_res_sub(msg):
    global on_recv
    global on_recv_flag

    obj_recv = json.loads(msg.data)
    if not 'res' in obj_recv.keys() or not 'status' in obj_recv.keys():
        msg_agent = {'status' : 'failed', 'reason' : 'receiver command unknown', 'node_name' : 'receiver', 'task_id' : -1}
    else:
        status = obj_recv['status']
        if status == 'failed':
            msg_agent = {'status' : 'failed', 'reason' : 'receiver execution failure', 'node_name' : 'receiver', 'task_id' : -1}
        elif status == 'success':
            on_recv = True
            on_recv_flag = True
            return
        else:
            msg_agent = {'status' : 'failed', 'reason' : 'receiver status unknown', 'node_name' : 'receiver', 'task_id' : -1}
    
    ros_agent_res_pub.publish(json.dumps(msg_agent))
    on_recv = True


on_send = False
on_send_flag = False
def on_send_res_sub(msg):
    global on_send
    global on_send_flag

    obj_send = json.loads(msg.data)
    if not 'res' in obj_send.keys() or not 'status' in obj_send.keys():
        msg_agent = {'status' : 'failed', 'reason' : 'sender command unknown', 'node_name' : 'sender', 'task_id' : -1}
    else:
        status = obj_send['status']
        if status == 'failed':
            msg_agent = {'status' : 'failed', 'reason' : 'sender execution failure', 'node_name' : 'sender', 'task_id' : -1}
        elif status == 'success':
            on_send = True
            on_send_flag = True
            return
        else:
            msg_agent = {'status' : 'failed', 'reason' : 'sender status unknown', 'node_name' : 'sender', 'task_id' : -1}

    ros_agent_res_pub.publish(json.dumps(msg_agent))
    on_send = True


sock_manage = None
def on_agent_manage_sub(msg):
    global on_send
    global on_send_flag
    global sock_manage

    obj_agent = json.loads(msg.data)

    if not 'cmd' in obj_agent.keys() or not 'node_name' in obj_agent.keys() or not 'task_id' in obj_agent.keys():
        msg_agent = {'status' : 'failed', 'reason' : 'env command unknown'}
    else:
        node_name = obj_agent['node_name']
        task_id = int(obj_agent['task_id'])

        sock_manage.sendall(msg.data.encode())
        print('message sent from ROS manager to ENV manager ...')

        msg_env = sock_manage.recv(2048).decode()
        print('message received from ENV manager to ROS manager ...')
        obj_env = json.loads(msg_env)
        if 'env' in obj_env.keys():
            if obj_env['env'] == 'launched':
                msg_send = {}
                msg_send['cmd'] = 'invoke_send'
                msg_send['host'] = obj_env['host']
                msg_send['port'] = int(obj_env['port'])
                ros_send_config_pub.publish(json.dumps(msg_send))
                print('ENV launched, message from ROS manager to Sender ...')

                while not on_send:
                    time.sleep(.001)
                on_send = False
                if on_send_flag:
                    msg_agent = {'status' : 'success', 'host' : obj_env['host'], 'port' : int(obj_env['port']), 'node_name' : node_name, 'task_id' : task_id}
                    on_send_flag = False
                    print('Sender config setup success!')
                else:
                    msg_agent = {'status' : 'failed', 'reason' : 'sender configuration failed', 'node_name' : node_name, 'task_id' : task_id}
                    print('Sender config failed ...')
            elif obj_env['env'] == 'failed':
                msg_agent = {'status' : 'failed', 'reason' : 'env can not be launched', 'node_name' : node_name, 'task_id' : task_id}
        elif 'close' in obj_env.keys():
            if obj_env['close'] == 'closed':
                msg_send = {}
                msg_send['cmd'] = 'close_send'
                msg_send['host'] = obj_env['host']
                msg_send['port'] = int(obj_env['port'])
                ros_send_config_pub.publish(json.dumps(msg_send))
                print('ENV closed, message from ROS manager to Sender ...')

                while not on_send:
                    time.sleep(.001)
                on_send = False
                if on_send_flag:
                    msg_agent = {'status' : 'closed', 'node_name' : node_name, 'task_id' : task_id}
                    on_send_flag = False
                    print('Sender config setup success!')
                else:
                    msg_agent = {'status' : 'failed', 'reason' : 'sender configuration failed', 'node_name' : node_name, 'task_id' : task_id}
                    print('Sender config failed ...')
        else:
            msg_agent = {'status' : 'failed', 'reason' : 'manage command can not be recognized', 'node_name' : node_name, 'task_id' : task_id}
    ros_agent_res_pub.publish(json.dumps(msg_agent))
    print('message from ROS manager to Agent ...')


# set of ROS communication handler
ros_recv_config_pub = rospy.Publisher('/receiver_config', String, queue_size=1)
ros_send_config_pub = rospy.Publisher('/sender_config', String, queue_size=1)
ros_agent_res_pub = rospy.Publisher('/agent_res', String, queue_size=1)
ros_recv_res_sub = rospy.Subscriber('/receiver_res', String, callback=on_recv_res_sub, queue_size=1)
ros_send_res_sub = rospy.Subscriber('/sender_res', String, callback=on_send_res_sub, queue_size=1)
ros_agent_manage_sub = rospy.Subscriber('/agent_manage', String, callback=on_agent_manage_sub, queue_size=1000)


def parse_params(config_file):
    cparser = configparser.ConfigParser()
    cparser.read(config_file)

    bridge_params_dict['host_vrenv'] = cparser['VRGYMENV_MANAGER']['HOST']
    bridge_params_dict['port_vrenv'] = int(cparser['VRGYMENV_MANAGER']['PORT'])
    bridge_params_dict['host_ros_recv'] = cparser['ROS_RECEIVER']['HOST']
    bridge_params_dict['port_ros_recv'] = int(cparser['ROS_RECEIVER']['PORT'])


def ros_recv_port_config():
    global on_recv
    global on_recv_flag

    msg_recv = {}
    msg_recv['cmd'] = 'init_recv_port'
    msg_recv['port'] = bridge_params_dict['port_ros_recv']
    ros_recv_config_pub.publish(json.dumps(msg_recv))
    print('message from ROS manager to Receiver ...')
    while not on_recv:
        time.sleep(.001)
    if on_recv_flag:
        on_recv_flag = False
        print('Receiver config setup success!')
        return True
    else:
        print('Receiver config failed ...')
        return False


def comm_ros():
    global sock_manage

    # suspend until related ROS nodes ready
    while ros_recv_config_pub.get_num_connections() == 0:
        time.sleep(.001)

    while ros_send_config_pub.get_num_connections() == 0:
        time.sleep(.001)

    if(ros_recv_port_config()):
        host = bridge_params_dict['host_vrenv']
        port = int(bridge_params_dict['port_vrenv'])
        sock_manage = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock_manage.connect((host, port))
        time.sleep(.001)

        rospy.spin()
    else:
        sys.exit(-1)


if __name__ == '__main__':
    config_file = 'bridge_config.ini'
    parse_params(config_file)

    rospy.init_node('bridge_manager', anonymous=True)
    comm_ros()