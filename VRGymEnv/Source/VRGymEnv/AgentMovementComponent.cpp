// Fill out your copyright notice in the Description page of Project Settings.

#include "AgentMovementComponent.h"

void UAgentMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UAgentMovementComponent::SafeMoveComponent(FVector Motion, float MovementSpeed, float DeltaTime)
{
	bool SafeMove = true;
	FVector DesiredMovementThisFrame = Motion;
	if (!DesiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult Hit;
		SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);
		if (Hit.IsValidBlockingHit())
		{
			SafeMove = false;
			SlideAlongSurface(DesiredMovementThisFrame, 1.0f - Hit.Time, Hit.Normal, Hit);
		}
	}
	return SafeMove;
}