// Fill out your copyright notice in the Description page of Project Settings.

#include "MessageWrapper.h"


// Add default functionality here for any IMessageWrapper functions that are not pure virtual.
std::string IMessageWrapper::RLMazeResetMsgWrapper(FString AgentName, std::string State, uint64 TimeStamp)
{
	return 0;
}

std::string IMessageWrapper::RLMazeStepMsgWrapper(FString AgentName, std::string NextState, float Reward, bool Terminal, uint64 TimeStamp)
{
	return 0;
}