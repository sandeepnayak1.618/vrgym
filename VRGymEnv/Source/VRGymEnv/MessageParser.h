// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "UObject/Interface.h"

#include "messages/transform.pb.h"
#include "messages/rl_maze.pb.h"
#include <string>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "MessageParser.generated.h"

using namespace rapidjson;


// This class does not need to be modified.
UINTERFACE()
class UMessageParser : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class VRGYMENV_API IMessageParser
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual bool RLMazeResetMsgParser(std::string Message, FString AgentName, uint64* TimeStamp, bool* Reply);
	virtual bool RLMazeStepMsgParser(std::string Message, FString AgentName, int ActionDim, float* AgentAction, uint64* TimeStamp, bool* Reply);

	virtual bool MotionPlanMsgParser(std::string Message, FString AgentName, TArray<TTuple<FString, FTransform> > &AgentJointsTransforms);
};
