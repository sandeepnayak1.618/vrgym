// Fill out your copyright notice in the Description page of Project Settings.

#include "CentralCommunicator.h"


void ACentralCommunicator::SocketInit()
{
	// listener socket
	TArray<FString> HostIPFields;
	_HostEnv.ParseIntoArray(HostIPFields, TEXT("."), true);
	FIPv4Endpoint Endpoint(FIPv4Address(FCString::Atoi(*HostIPFields[0]), FCString::Atoi(*HostIPFields[1]), FCString::Atoi(*HostIPFields[2]), FCString::Atoi(*HostIPFields[3])), _PortEnv);
	_SockListener = FTcpSocketBuilder(TEXT("EnvListener"))
		.AsReusable()
		.BoundToEndpoint(Endpoint)
		.WithReceiveBufferSize(COMM_BUFFER_SIZE)
		.WithSendBufferSize(COMM_BUFFER_SIZE)
		.Listening(1);

	// peer socket
	BuildListenerConnection();

	// sender socket
	_SockSender = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("EnvSender"), false);
	TArray<FString> RecvIPFields;
	_HostRecv.ParseIntoArray(RecvIPFields, TEXT("."), true);
	FIPv4Address IPSender(FCString::Atoi(*RecvIPFields[0]), FCString::Atoi(*RecvIPFields[1]), FCString::Atoi(*RecvIPFields[2]), FCString::Atoi(*RecvIPFields[3]));
	TSharedRef<FInternetAddr> AddrSender = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	AddrSender->SetIp(IPSender.Value);
	AddrSender->SetPort(_PortRecv);
	_SenderConnected = _SockSender->Connect(*AddrSender);
	if(_SenderConnected)
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Sender Connected")));
}

void ACentralCommunicator::SocketDestroy()
{
	if (_DelegateReceiver != NULL)
	{
		delete _DelegateReceiver;
		_DelegateReceiver = NULL;
	}

	if (_TcpListener != NULL)
	{
		delete _TcpListener;
		_TcpListener = NULL;
	}

	if (_SockListener != NULL)
	{
		ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_SockListener);
		_SockListener = NULL;
	}

	if (_SockPeer != NULL)
	{
		ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_SockPeer);
		_SockPeer = NULL;
	}

	if (_SockSender != NULL)
	{
		ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_SockSender);
	}
}

// Sets default values
ACentralCommunicator::ACentralCommunicator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACentralCommunicator::BeginPlay()
{
	Super::BeginPlay();
	
	_RunningTime = 0.0f;
	_GlobalMessageQueue.Empty();
	_AgentMessageQueue.Empty();

	// parse arguments for endpoint communication
	FString Cmd = FCommandLine::Get();
	TArray<FString> Fields;
	Cmd.ParseIntoArray(Fields, TEXT(" "), true);

	int HostIdx = Fields.Find(TEXT("-host"));
	_HostEnv = Fields[HostIdx + 1];
	int PortIdx = Fields.Find(TEXT("-port"));
	_PortEnv = FCString::Atoi(*Fields[PortIdx + 1]);

	int HostRecvIdx = Fields.Find(TEXT("-host_ros_recv"));
	_HostRecv = Fields[HostRecvIdx + 1];
	int PortRecvIdx = Fields.Find(TEXT("-port_ros_recv"));
	_PortRecv = FCString::Atoi(*Fields[PortRecvIdx + 1]);

	// initialize sockets for central communication
	SocketInit();

	// initialize map for agents communication
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABaseAgent::StaticClass(), FoundActors);
	for (auto Actor : FoundActors)
	{
		ABaseAgent* BaseAgent = Cast<ABaseAgent>(Actor);
		if (BaseAgent)
		{
			//_Name2AgentMap.Emplace(BaseAgent->GetActorLabel(), BaseAgent);
			FString AgentName = BaseAgent->GetName();
			_Name2AgentMap.Emplace(AgentName.LeftChop(AgentName.Len()-AgentName.Find(TEXT("_"))), BaseAgent);
		}
	}

	FTimerHandle CommAnimHandler;
	GetWorldTimerManager().SetTimer(CommAnimHandler, this, &ACentralCommunicator::CentralAnimLaunch, 0.005f, true);
}

// Called when the game stops
void ACentralCommunicator::BeginDestroy()
{
	Super::BeginDestroy();

	SocketDestroy();
}

// Called every frame
void ACentralCommunicator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	_RunningTime += DeltaTime;

}

void ACentralCommunicator::CentralAnimLaunch()
{
	GlobalMessageProc(_RunningTime);
	AgentMessageProc(_RunningTime);
}

void ACentralCommunicator::GlobalMessageProc(float Time)
{
	FString Msg;
	if (!_GlobalMessageQueue.IsEmpty())
	{
		_GlobalMessageQueue.Dequeue(Msg);
		//if (_GlobalMessageQueue.IsEmpty())
		//	_GlobalMessageQueue.Enqueue(Msg);

		// Process section
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(*Msg));

		// Response section
		if (_SenderConnected)
		{
			Msg += "\n";
			TCHAR* TCHAR_Msg = Msg.GetCharArray().GetData();

			int32 Sent = 0;
			bool Success = _SockSender->Send((uint8*)TCHAR_TO_UTF8(TCHAR_Msg), FCString::Strlen(TCHAR_Msg), Sent);
		}
	}
}

void ACentralCommunicator::AgentMessageProc(float Time)
{
	FString Msg;
	if (!_AgentMessageQueue.IsEmpty())
	{
		_AgentMessageQueue.Dequeue(Msg);

		if (_SenderConnected)
		{
			Msg += "\n";
			TCHAR* TCHAR_Msg = Msg.GetCharArray().GetData();

			int32 Sent = 0;
			bool Success = _SockSender->Send((uint8*)TCHAR_TO_UTF8(TCHAR_Msg), FCString::Strlen(TCHAR_Msg), Sent);
		}
	}
}

bool ACentralCommunicator::AgentMessageForward(FString Msg)
{
	if (!_AgentMessageQueue.Enqueue(Msg))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Failed To Receive Message From Agent")));
		return false;
	}

	return true;
}

void ACentralCommunicator::BuildListenerConnection()
{
	if (_SockListener != NULL)
	{
		_TcpListener = new FTcpListener(*_SockListener);
		_TcpListener->OnConnectionAccepted().BindUObject(this, &ACentralCommunicator::OnTcpListenerConnectionAccepted);

		if (_TcpListener->Init())
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Initialize Env Listener Success!")));
			_ClientAccepting = true;
		}
	}
	else GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Initialize Env Listener Failed!")));
}

bool ACentralCommunicator::OnTcpListenerConnectionAccepted(FSocket* Socket, const FIPv4Endpoint& Endpoint)
{
	if (_ClientAccepting)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Incoming Connection!")));
		auto State = Socket->GetConnectionState();
		if (State == ESocketConnectionState::SCS_Connected)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Connected!")));
		}

		_SockPeer = Socket;
		_DelegateReceiver = new TCPReceiver(Socket, COMM_BUFFER_SIZE);
		_DelegateReceiver->OnTcpReceive().BindUObject(this, &ACentralCommunicator::OnTcpReceived);
		_DelegateReceiver->Init();
		return true;
	}
	return false;
}

int32 ACentralCommunicator::OnTcpReceived(const uint8* Data, int32 BytesReceived)
{
	static uint8 Buffer[COMM_BUFFER_SIZE];

	int32 Consumed = 0;
	int32 Previous = 0;

	for (int i = 0; i < BytesReceived; i++)
	{
		if (Data[i] == '\n')
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Data Received From Client!")));
			FGenericPlatformMemory::Memmove(reinterpret_cast<void*>(Buffer), reinterpret_cast<const void*>(Data + Previous), i - Previous);
			Buffer[i - Previous] = 0;

			FString FStrData(ANSI_TO_TCHAR(reinterpret_cast<const char*>(Buffer)));
			std::string StrData(TCHAR_TO_ANSI(*FStrData));

			Document Doc;
			Doc.Parse(StrData.c_str());
			std::string AgentName = Doc["agent_name"].GetString();
			ABaseAgent* BaseAgent = _Name2AgentMap[FString(AgentName.c_str())];
			if (BaseAgent)
				BaseAgent->RecvAgentData(StrData);
			//if (!_GlobalMessageQueue.Enqueue(FStrData))
			//{
			//	GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Central Communicator Enqueue Failed!")));
			//}

			Consumed = i + 1;
			Previous = i + 1;
		}
	}

	return Consumed;
}