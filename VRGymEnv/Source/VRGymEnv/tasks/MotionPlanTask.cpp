// Fill out your copyright notice in the Description page of Project Settings.

#include "MotionPlanTask.h"
#include <google/protobuf/text_format.h>

// Utilities to transform right handed coordinate to left handed
static FVector VectorSwitchY(FVector ru)
{
	return FVector(ru.X, -(ru.Y), ru.Z);
}

static FQuat QuaternionSwitchY(FQuat rq)
{
	return FQuat(rq.X, -(rq.Y), rq.Z, -(rq.W));
}

UMotionPlanTask::UMotionPlanTask()
{
}

void UMotionPlanTask::MotionPlanTransform(ABaseAgent* MotionPlanAgent, 
	TMap<FString, UStaticMeshComponent*> &NameToMeshMap, 
	TArray<TTuple<FString, FTransform> > &AgentJointsTransforms)
{
	for (int i = 0; i < AgentJointsTransforms.Num(); i++)
	{
		auto JointTuple = AgentJointsTransforms[i];
		FString ComponentName = (FString)JointTuple.Get<0>();
		FTransform ComponentTransform = (FTransform)JointTuple.Get<1>();

		FVector ComponentVec = VectorSwitchY(ComponentTransform.GetTranslation());
		FQuat ComponentQuat = QuaternionSwitchY(ComponentTransform.GetRotation());

		UStaticMeshComponent* ComponentMesh = (UStaticMeshComponent*)NameToMeshMap[ComponentName];
		ComponentMesh->SetRelativeLocation(ComponentVec);
		ComponentMesh->SetRelativeRotation(ComponentQuat);
	}
}

bool UMotionPlanTask::MotionPlanMsgParser(std::string Message, FString AgentName, TArray<TTuple<FString, FTransform> > &AgentJointsTransforms)
{
	Transform::JointsTransformMsg AgentJointsTransformMsg;

	if (!google::protobuf::TextFormat::ParseFromString(Message, &AgentJointsTransformMsg))
		return false;

	for (int i = 0; i < AgentJointsTransformMsg.joints_transform_size(); i++)
	{
		const Transform::JointsTransformMsg::JointMsg AgentJointMsg = AgentJointsTransformMsg.joints_transform(i);
		std::string AgentJointName = AgentJointMsg.joint_name();
		Transform::PoseMsg AgentJointPose = AgentJointMsg.pose();

		Transform::VectorMsg AgentVec = AgentJointPose.vector();
		Transform::QuatMsg AgentQuat = AgentJointPose.quat();

		FVector AgentJointTFVec; FQuat AgentJointTFQuat;
		AgentJointTFVec.X = AgentVec.x();
		AgentJointTFVec.Y = AgentVec.y();
		AgentJointTFVec.Z = AgentVec.z();
		AgentJointTFQuat.X = AgentQuat.x();
		AgentJointTFQuat.Y = AgentQuat.y();
		AgentJointTFQuat.Z = AgentQuat.z();
		AgentJointTFQuat.W = AgentQuat.w();

		FTransform AgentJointTF(AgentJointTFQuat, AgentJointTFVec, FVector(1.0f));
		AgentJointsTransforms.Emplace(MakeTuple(FString(AgentJointName.c_str()), AgentJointTF));
	}

	return true;
}
