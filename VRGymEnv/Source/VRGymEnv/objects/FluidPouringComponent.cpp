// Fill out your copyright notice in the Description page of Project Settings.

#include "FluidPouringComponent.h"
#include "PourContainerObject.h"

#define POUR_TRACE ECollisionChannel::ECC_GameTraceChannel1

void UFluidPouringComponent::PourFluid(FLinearColor FluidColor, float PourAmount)
{
	UWorld* TheWorld = this->GetWorld();

	TArray<FHitResult> OutResults;
	FCollisionShape GrabSphere = FCollisionShape::MakeSphere(1.f);
	FCollisionObjectQueryParams ObjectParams;
	FCollisionQueryParams CollisionParams;
	ObjectParams.AddObjectTypesToQuery(ECollisionChannel::ECC_WorldDynamic);
	ObjectParams.AddObjectTypesToQuery(ECollisionChannel::ECC_PhysicsBody);
	ObjectParams.AddObjectTypesToQuery(ECollisionChannel::ECC_Visibility);
	CollisionParams.bFindInitialOverlaps = false;
	CollisionParams.AddIgnoredActor(this->GetOwner());

	float SweepDist = 200.0;
	TheWorld->SweepMultiByChannel(OutResults, GetComponentLocation(), GetComponentLocation() + FVector(0, 0, -SweepDist), FQuat::Identity, POUR_TRACE, GrabSphere, CollisionParams);

	for (int i = 0; i < OutResults.Num(); i++)
	{
		APourContainerObject* ReceivingContainer = Cast<APourContainerObject>(OutResults[i].GetActor());
		if (ReceivingContainer)
		{
			ReceivingContainer->RecieveFluid(FluidColor, PourAmount, FluidKind);
			break;
		}

	}
}

