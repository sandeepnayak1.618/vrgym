// Fill out your copyright notice in the Description page of Project Settings.

#include "CoffeeMakerObject.h"


// Sets default values
ACoffeeMakerObject::ACoffeeMakerObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bIsOn = false;
	bIsPouringCoffee = false;
	MaterialIndex = 4;
	NozzleSize = 1.f;

	CoffeeMakerMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("CoffeeMakerMesh"));
	RootComponent = CoffeeMakerMesh;

	PowerButton = CreateDefaultSubobject<USphereComponent>(FName("PowerButton"));
	PourCoffeeButton = CreateDefaultSubobject<USphereComponent>(FName("PourCoffeeButton"));
	PowerButtonLight = CreateDefaultSubobject<UDecalComponent>(FName("PowerButtonLight"));
	CoffeeNozzle = CreateDefaultSubobject<UFluidPouringComponent>(FName("CoffeeNozzle"));

	PowerButton->SetupAttachment(CoffeeMakerMesh);
	PourCoffeeButton->SetupAttachment(CoffeeMakerMesh);
	PowerButtonLight->SetupAttachment(PowerButton);
	CoffeeNozzle->SetupAttachment(CoffeeMakerMesh);

	LightTimer = 0;
	CoffeeTimer = 0;
	PourTime = 5;
}

// Called when the game starts or when spawned
void ACoffeeMakerObject::BeginPlay()
{
	Super::BeginPlay();
	
	CoffeeMakerMesh->SetMaterial(MaterialIndex, bIsOn ? NeutralOnMaterial : OffMaterial);
	CoffeeNozzle->SetColorParameter(FName("FluidColor"), CoffeeColor);
	CoffeeNozzle->SetFloatParameter(FName("MouthRadius"), NozzleSize);
	CoffeeNozzle->Deactivate();

}

// Called every frame
void ACoffeeMakerObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Animation(DeltaTime);
}

void ACoffeeMakerObject::Animation(float DeltaTime)
{
	if (!bIsOn)
	{
		LightTimer += DeltaTime;
		if (LightTimer < 2.0)
		{
			PowerButton->SetVisibility(false);
		}
		else if (LightTimer < 2.5)
		{
			PowerButton->SetVisibility(true);
		}
		else
		{
			LightTimer -= 2.5;
		}
	}

	if (bIsPouringCoffee)
	{
		CoffeeTimer += DeltaTime;
		if (CoffeeTimer < 5.0)
		{

		}
		else if (CoffeeTimer < 5.0 + PourTime)
		{
			CoffeeNozzle->Activate();
			CoffeeNozzle->PourFluid(CoffeeColor, DeltaTime*0.1);
		}
		else
		{
			CoffeeMakerMesh->SetMaterial(MaterialIndex, NeutralOnMaterial);
			CoffeeNozzle->Deactivate();
			CoffeeTimer = 0.0f;
			bIsPouringCoffee = false;
		}
	}
}

void ACoffeeMakerObject::ToggleMachineOnOff(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!bIsPouringCoffee)
	{
		bIsOn = !bIsOn;
		CoffeeMakerMesh->SetMaterial(MaterialIndex, bIsOn ? NeutralOnMaterial : OffMaterial);
		PowerButtonLight->SetVisibility(!bIsOn);
	}
}

void ACoffeeMakerObject::PourCoffee(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bIsOn)
	{
		bIsPouringCoffee = true;
		CoffeeMakerMesh->SetMaterial(MaterialIndex, PouringMaterial);
	}
}