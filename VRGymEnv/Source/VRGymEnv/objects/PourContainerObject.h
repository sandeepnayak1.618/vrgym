// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "BaseObject.h"
#include "FluidPouringComponent.h"
#include "GameFramework/Actor.h"
#include "PourContainerObject.generated.h"

UCLASS()
class VRGYMENV_API APourContainerObject : public ABaseObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APourContainerObject();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, Category = "PourContainer")
		UArrowComponent* UpArrow;

	UPROPERTY(EditDefaultsOnly, Category = "PourContainer")
		UStaticMeshComponent* ContainerMesh;

	UPROPERTY(EditDefaultsOnly, Category = "PourContainer")
		UStaticMeshComponent* FillMesh;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		UFluidPouringComponent* FluidParticles;

	UPROPERTY(BlueprintReadOnly, Category = "PourContainer")
		float MaxFillFraction;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		float CurrentFillFraction;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		float RemainingFluid;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		float MouthRadius;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		float FlowRate;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		bool bIsInfinite;

	UPROPERTY(EditAnywhere, Category = "PourContainer")
		FLinearColor FluidColor;

	//UPROPERTY(EditAnywhere, Category = "PourContainer", Meta = (BlueprintProtected = "true"))
	//	FString Item_ID;

	UFUNCTION(BlueprintCallable, Category = "PourContainer")
		void RecieveFluid(FLinearColor NewFluidColor, float FluidAmount, FString FluidKind);

private:
	virtual void Animation(float DeltaTime) override;

	TArray<FString> ContainedFluids;
	UMaterialInstanceDynamic* FluidMaterialRef;
};
