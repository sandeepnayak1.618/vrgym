// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/Actor.h"

#include "BaseAgent.h"
#include "Runtime/Core/Public/HAL/ThreadingBase.h"
#include "Networking.h"
#include "TCPReceiver.h"

#include <string>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "CentralCommunicator.generated.h"

using namespace rapidjson;

#define COMM_BUFFER_SIZE 20000

// CentralCommunicator Controls Env Agents Communication with ROS Bridge
UCLASS()
class VRGYMENV_API ACentralCommunicator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACentralCommunicator();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called when the game stops
	virtual void BeginDestroy() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	bool AgentMessageForward(FString Msg);

private:
	// Communication
	FSocket* _SockListener;
	FSocket* _SockPeer;
	FSocket* _SockSender;

	FTcpListener* _TcpListener;
	TCPReceiver* _DelegateReceiver;
	
	FString _HostEnv;
	int _PortEnv;
	FString _HostRecv;
	int _PortRecv;

	bool _ClientAccepting;
	bool _SenderConnected;

	void BuildListenerConnection();
	bool OnTcpListenerConnectionAccepted(FSocket* Socket, const FIPv4Endpoint& Endpoint);
	int32 OnTcpReceived(const uint8* Data, int32 BytesReceived);

	void SocketInit();
	void SocketDestroy();

	// Streaming data 
	TQueue<FString> _GlobalMessageQueue;
	void GlobalMessageProc(float Time);
	TQueue<FString> _AgentMessageQueue;
	void AgentMessageProc(float Time);

	// Agent coordination
	TMap<FString, ABaseAgent*> _Name2AgentMap;

	// Timer animation process
	float _RunningTime;
	void CentralAnimLaunch();
};
