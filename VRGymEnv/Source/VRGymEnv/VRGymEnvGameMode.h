// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/GameMode.h"

#include "CentralCommunicator.h"

#include "VRGymEnvGameMode.generated.h"

/**
 * 
 */
UCLASS()
class VRGYMENV_API AVRGymEnvGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AVRGymEnvGameMode();

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void Tick(float DeltaSeconds) override;

	ACentralCommunicator* GetCentralCommunicator() const;

private:
	ACentralCommunicator* _GameAgentCommunicator;

};
