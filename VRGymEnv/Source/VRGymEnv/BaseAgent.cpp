// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseAgent.h"
#include "CentralCommunicator.h"
#include "VRGymEnvGameMode.h"


// Sets default values
ABaseAgent::ABaseAgent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_Camera = CreateDefaultSubobject<UCameraComponent>("camera");
	_MovementComponent = CreateDefaultSubobject<UAgentMovementComponent>("movement_component");
}

void ABaseAgent::InitializeTransMap(UStaticMeshComponent** C, const TCHAR* Name, const TCHAR* MeshPath)
{}

void ABaseAgent::InitializeComponent()
{}

// Called when the game starts or when spawned
void ABaseAgent::BeginPlay()
{
	Super::BeginPlay();
	
	_TimeStamp = 0;
	_PendingData.Empty();

	//FTimerHandle AgentAnimHandler;
	//GetWorldTimerManager().SetTimer(AgentAnimHandler, this, &ABaseAgent::LaunchAnimation, 0.005f, true);
}

// Called every frame
void ABaseAgent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseAgent::LaunchAnimation()
{
	ParseAgentData();
}

void ABaseAgent::ParseAgentData()
{
	std::string AgentData;
	if (!_PendingData.IsEmpty())
	{
		_PendingData.Dequeue(AgentData);

		AVRGymEnvGameMode* CurrentGameMode = (AVRGymEnvGameMode*)GetWorld()->GetAuthGameMode();
		ACentralCommunicator* Comm = CurrentGameMode->GetCentralCommunicator();
		Comm->AgentMessageForward(FString(AgentData.c_str()));
	}
}

void ABaseAgent::RecvAgentData(std::string AgentData)
{
	_PendingData.Enqueue(AgentData);
}

void ABaseAgent::SetTaskMsg(std::string Msg)
{
	_TaskMsg = Msg;
}

uint64 ABaseAgent::GetTimeStamp() const
{
	return _TimeStamp;
}
