// Fill out your copyright notice in the Description page of Project Settings.

#include "BaxterAgent.h"
#include "CentralCommunicator.h"
#include "VRGymEnvGameMode.h"


// Utilities to transform right handed coordinate to left handed
static FVector VectorSwitchY(FVector ru) 
{
	return FVector(ru.X, -(ru.Y), ru.Z);
}

static FQuat QuaternionSwitchY(FQuat rq)
{
	return FQuat(rq.X, -(rq.Y), rq.Z, -(rq.W));
}

ABaxterAgent::ABaxterAgent()
{
	InitializeComponent();

	_TaskMazeRL = CreateDefaultSubobject<UMazeRLTask>("maze_rl");
	_TaskMotionPlan = CreateDefaultSubobject<UMotionPlanTask>("motion_plan");
}

static void LoadInMesh(UStaticMeshComponent** C, const TCHAR* Path) 
{
	UStaticMesh* pMesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), NULL, Path));
	if (pMesh == NULL) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Can't find the mesh"));
	}
	if (!(*C)->SetStaticMesh(pMesh)) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Fail setting the mesh"));
	}
}

void ABaxterAgent::InitializeTransMap(UStaticMeshComponent** C, const TCHAR* Name, const TCHAR* MeshPath)
{
	UE_LOG(LogTemp, Warning, TEXT("Start initializing frame for %s"), Name);
	LoadInMesh(C, MeshPath);
	_NameToMeshMap.Emplace(Name, *C);
}

void ABaxterAgent::InitializeComponent()
{
	_BodySphere = CreateDefaultSubobject<USphereComponent>("body_sphere");
	_BodySphere->InitSphereRadius(0.1f);
	_BodySphere->SetCollisionProfileName(TEXT("Pawn"));

	RootComponent = _BodySphere;

	_Base = CreateDefaultSubobject<UStaticMeshComponent>("base");
	InitializeTransMap(&_Base,
		TEXT("base"),
		TEXT("StaticMesh'/Game/Robots/Baxter/PEDESTAL.PEDESTAL'")
	);
	_Base->SetWorldScale3D(FVector(1.0f)*100.0f);
	_Base->SetupAttachment(_BodySphere);

	_Torso = CreateDefaultSubobject<UStaticMeshComponent>("torso");
	InitializeTransMap(&_Torso,
		TEXT("torso"),
		TEXT("StaticMesh'/Game/Robots/Baxter/base_link.base_link'")
	);
	_Torso->SetupAttachment(_Base);

	_Head = CreateDefaultSubobject<UStaticMeshComponent>("head");
	InitializeTransMap(&_Head,
		TEXT("head"),
		TEXT("StaticMesh'/Game/Robots/Baxter/H0.H0'")
	);
	_Head->SetupAttachment(_Torso);

	_Screen = CreateDefaultSubobject<UStaticMeshComponent>("screen");
	InitializeTransMap(&_Screen,
		TEXT("screen"),
		TEXT("StaticMesh'/Game/Robots/Baxter/H1.H1'")
	);
	_Screen->SetupAttachment(_Head);

	_LArmMount = CreateDefaultSubobject<UStaticMeshComponent>("left_arm_mount");
	InitializeTransMap(&_LArmMount,
		TEXT("left_arm_mount"),
		TEXT("")
	);
	_LArmMount->SetupAttachment(_Torso);

	_LUShoulder = CreateDefaultSubobject<UStaticMeshComponent>("left_upper_shoulder");
	InitializeTransMap(&_LUShoulder,
		TEXT("left_upper_shoulder"),
		TEXT("StaticMesh'/Game/Robots/Baxter/S0.S0'")
	);
	_LUShoulder->SetupAttachment(_LArmMount);

	_LLShoulder = CreateAbstractDefaultSubobject<UStaticMeshComponent>("left_lower_shoulder");
	InitializeTransMap(&_LLShoulder,
		TEXT("left_lower_shoulder"),
		TEXT("StaticMesh'/Game/Robots/Baxter/S1.S1'")
	);
	_LLShoulder->SetupAttachment(_LUShoulder);

	_LUElbow = CreateDefaultSubobject<UStaticMeshComponent>("left_upper_elbow");
	InitializeTransMap(&_LUElbow,
		TEXT("left_upper_elbow"),
		TEXT("StaticMesh'/Game/Robots/Baxter/E0.E0'")
	);
	_LUElbow->SetupAttachment(_LLShoulder);

	_LLElbow = CreateDefaultSubobject<UStaticMeshComponent>("left_lower_elbow");
	InitializeTransMap(&_LUElbow,
		TEXT("left_lower_elbow"),
		TEXT("StaticMesh'/Game/Robots/Baxter/E1.E1'")
	);
	_LLElbow->SetupAttachment(_LUElbow);

	_LUForearm = CreateDefaultSubobject<UStaticMeshComponent>("left_upper_forearm");
	InitializeTransMap(&_LUForearm,
		TEXT("left_upper_forearm"),
		TEXT("StaticMesh'/Game/Robots/Baxter/W0.W0'")
	);
	_LUForearm->SetupAttachment(_LLElbow);

	_LLForearm = CreateDefaultSubobject<UStaticMeshComponent>("left_lower_forearm");
	InitializeTransMap(&_LLForearm,
		TEXT("left_lower_forearm"),
		TEXT("StaticMesh'/Game/Robots/Baxter/W1.W1'")
	);
	_LLForearm->SetupAttachment(_LUForearm);

	_LWrist = CreateDefaultSubobject<UStaticMeshComponent>("left_wrist");
	InitializeTransMap(&_LWrist,
		TEXT("left_wrist"),
		TEXT("StaticMesh'/Game/Robots/Baxter/W2.W2'")
	);
	_LWrist->SetupAttachment(_LLForearm);

	_RArmMount = CreateDefaultSubobject<UStaticMeshComponent>("right_arm_mount");
	InitializeTransMap(&_RArmMount,
		TEXT("right_arm_mount"),
		TEXT("")
	);
	_RArmMount->SetupAttachment(_Torso);

	_RUShoulder = CreateDefaultSubobject<UStaticMeshComponent>("right_upper_shoulder");
	InitializeTransMap(&_RUShoulder,
		TEXT("right_upper_shoulder"),
		TEXT("StaticMesh'/Game/Robots/Baxter/S0.S0'")
	);
	_RUShoulder->SetupAttachment(_RArmMount);

	_RLShoulder = CreateDefaultSubobject<UStaticMeshComponent>("right_lower_shoulder");
	InitializeTransMap(&_RLShoulder,
		TEXT("right_lower_shoulder"),
		TEXT("StaticMesh'/Game/Robots/Baxter/S1.S1'")
	);
	_RLShoulder->SetupAttachment(_RUShoulder);

	_RUElbow = CreateDefaultSubobject<UStaticMeshComponent>("right_upper_elbow");
	InitializeTransMap(&_RUElbow,
		TEXT("right_upper_elbow"),
		TEXT("StaticMesh'/Game/Robots/Baxter/E0.E0'")
	);
	_RUElbow->SetupAttachment(_RLShoulder);

	_RLElbow = CreateDefaultSubobject<UStaticMeshComponent>("right_lower_elbow");
	InitializeTransMap(&_RLElbow,
		TEXT("right_lower_elbow"),
		TEXT("StaticMesh'/Game/Robots/Baxter/E1.E1'")
	);
	_RLElbow->SetupAttachment(_RUElbow);

	_RUForearm = CreateDefaultSubobject<UStaticMeshComponent>("right_upper_forearm");
	InitializeTransMap(&_RUForearm,
		TEXT("right_upper_forearm"),
		TEXT("StaticMesh'/Game/Robots/Baxter/W0.W0'")
	);
	_RUForearm->SetupAttachment(_RLElbow);

	_RLForearm = CreateDefaultSubobject<UStaticMeshComponent>("right_lower_forearm");
	InitializeTransMap(&_RLForearm,
		TEXT("right_lower_forearm"),
		TEXT("StaticMesh'/Game/Robots/Baxter/W1.W1'")
	);
	_RLForearm->SetupAttachment(_RUForearm);

	_RWrist = CreateDefaultSubobject<UStaticMeshComponent>("right_wrist");
	InitializeTransMap(&_RWrist,
		TEXT("right_wrist"),
		TEXT("StaticMesh'/Game/Robots/Baxter/W2.W2'")
	);
	_RWrist->SetupAttachment(_RLForearm);

	_Camera->SetupAttachment(_Head);
	_Camera->SetRelativeRotation(FRotator(-15.0, 0.0, 0.0));
	_Camera->SetWorldScale3D(FVector(1.0f)*0.1);

	_SceneCaptureComponent2D = CreateDefaultSubobject<USceneCaptureComponent2D>("scene_component");
	_SceneCaptureComponent2D->SetupAttachment(_Camera);

	_MovementComponent->UpdatedComponent = RootComponent;
}

void ABaxterAgent::BeginPlay()
{
	Super::BeginPlay();

	// Arguments for task
	FString Cmd = FCommandLine::Get();
	TArray<FString> Fields;
	Cmd.ParseIntoArray(Fields, TEXT(" "), true);

	int TaskNameIdx = Fields.Find(TEXT("-task"));
	FString TaskName = Fields[TaskNameIdx + 1];

	if (TaskName.Equals(TEXT("MAZERL")))
	{
		_ETask = EBaxterAgentTask::TASK_MAZERL;
		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Task Initialized!")));
	}
	else if (TaskName.Equals(TEXT("MOTIONPLAN")))
	{
		_ETask = EBaxterAgentTask::TASK_MOTIONPLAN;
		GEngine->AddOnScreenDebugMessage(-1, 5.0, FColor::Red, FString::Printf(TEXT("Task Initialized!")));
	}

	// Initialize agent pose
	SetActorLocation(FVector(-940.754883, 328.966675, 154.848007));
	SetActorRotation(FQuat(0.0, 0.0, 0.547618, 0.836728));

	// Initialize body parts pose
	_Head->SetRelativeLocation(VectorSwitchY(FVector(0.06, 0.0, 0.686)));
	_Head->SetRelativeRotation(QuaternionSwitchY(FQuat(0.0, 0.0, -0.015723, 0.999876)));

	_LArmMount->SetRelativeLocation(VectorSwitchY(FVector(0.025243, 0.219337, 0.107931)));
	_LArmMount->SetRelativeRotation(QuaternionSwitchY(FQuat(-0.003302, 0.001062, 0.383527, 0.923523)));

	_RArmMount->SetRelativeLocation(VectorSwitchY(FVector(0.025229, -0.219909, 0.108519)));
	_RArmMount->SetRelativeRotation(QuaternionSwitchY(FQuat(-0.001617, 0.001937, -0.380643, 0.924719)));

	_LUShoulder->SetRelativeLocation(VectorSwitchY(FVector(0.055695, 0.0, 0.011038)));
	_LUShoulder->SetRelativeRotation(QuaternionSwitchY(FQuat(0.0, 0.0, -0.144454, 0.989512)));

	_RUShoulder->SetRelativeLocation(VectorSwitchY(FVector(0.055695, 0.0, 0.011038)));
	_RUShoulder->SetRelativeRotation(QuaternionSwitchY(FQuat(0.0, 0.0, 0.123933, 0.992291)));

	_LLShoulder->SetRelativeLocation(VectorSwitchY(FVector(0.069, 0.0, 0.27035)));
	_LLShoulder->SetRelativeRotation(QuaternionSwitchY(FQuat(-0.679989, -0.193946, -0.193946, 0.679989)));

	_RLShoulder->SetRelativeLocation(VectorSwitchY(FVector(0.069, 0.0, 0.27035)));
	_RLShoulder->SetRelativeRotation(QuaternionSwitchY(FQuat(-0.705983, -0.039841, -0.039841, 0.705983)));

	_LUElbow->SetRelativeLocation(VectorSwitchY(FVector(0.102, 0.0, 0.0)));
	_LUElbow->SetRelativeRotation(QuaternionSwitchY(FQuat(0.241661, 0.66453, 0.241661, 0.66453)));

	_RUElbow->SetRelativeLocation(VectorSwitchY(FVector(0.102, 0.0, 0.0)));
	_RUElbow->SetRelativeRotation(QuaternionSwitchY(FQuat(0.650232, 0.277847, 0.650232, 0.277847)));

	_LLElbow->SetRelativeLocation(VectorSwitchY(FVector(0.069, 0.0, 0.26242)));
	_LLElbow->SetRelativeRotation(QuaternionSwitchY(FQuat(-0.69344, 0.138349, 0.138349, 0.69344)));

	_RLElbow->SetRelativeLocation(VectorSwitchY(FVector(0.069, 0.0, 0.26242)));
	_RLElbow->SetRelativeRotation(QuaternionSwitchY(FQuat(-0.704382, -0.062019, -0.062019, 0.704382)));

	_LUForearm->SetRelativeLocation(VectorSwitchY(FVector(0.10359, 0.0, 0.0)));
	_LUForearm->SetRelativeRotation(QuaternionSwitchY(FQuat(0.440465, 0.553164, 0.440465, 0.553164)));

	_RUForearm->SetRelativeLocation(VectorSwitchY(FVector(0.10359, 0.0, 0.0)));
	_RUForearm->SetRelativeRotation(QuaternionSwitchY(FQuat(0.037675, 0.706102, 0.037675, 0.706102)));

	_LLForearm->SetRelativeLocation(VectorSwitchY(FVector(0.01, 0.0, 0.2707)));
	_LLForearm->SetRelativeRotation(QuaternionSwitchY(FQuat(0.204354, 0.676934, 0.676934, -0.204354)));

	_RLForearm->SetRelativeLocation(VectorSwitchY(FVector(0.01, 0.0, 0.2707)));
	_RLForearm->SetRelativeRotation(QuaternionSwitchY(FQuat(0.273602, 0.652029, 0.652029, -0.273602)));

	_LWrist->SetRelativeLocation(VectorSwitchY(FVector(0.115975, 0.0, 0.0)));
	_LWrist->SetRelativeRotation(QuaternionSwitchY(FQuat(0.656351, 0.263064, 0.656351, 0.263064)));

	_RWrist->SetRelativeLocation(VectorSwitchY(FVector(0.115975, 0.0, 0.0)));
	_RWrist->SetRelativeRotation(QuaternionSwitchY(FQuat(0.551386, 0.442689, 0.551386, 0.442689)));

	uint8 FrameSize = _TaskMazeRL->GetFrameSize();
	_RenderTarget2D = NewObject<UTextureRenderTarget2D>();
	_RenderTarget2D->InitAutoFormat(FrameSize, FrameSize);

	_SceneCaptureComponent2D->TextureTarget = _RenderTarget2D;
	_SceneCaptureComponent2D->ProjectionType = ECameraProjectionMode::Type::Perspective;
	_SceneCaptureComponent2D->FOVAngle = 90.0;
	_SceneCaptureComponent2D->CaptureSource = ESceneCaptureSource::SCS_FinalColorLDR;
	_SceneCaptureComponent2D->CompositeMode = ESceneCaptureCompositeMode::SCCM_Overwrite;
	_SceneCaptureComponent2D->bCaptureOnMovement = true;
	_SceneCaptureComponent2D->bCaptureEveryFrame = false;
	_SceneCaptureComponent2D->MaxViewDistanceOverride = -1.0;
	_SceneCaptureComponent2D->bAutoActivate = true;
	_SceneCaptureComponent2D->DetailMode = EDetailMode::DM_High;

	ABaseAgent* BaseAgentInstance = Cast<ABaseAgent>(this);
	if(BaseAgentInstance) _TaskMazeRL->MazeTaskInit(BaseAgentInstance);
	_TaskMazeRL->SetRenderTarget(_RenderTarget2D);

	FTimerHandle AgentAnimHandler;
	GetWorldTimerManager().SetTimer(AgentAnimHandler, this, &ABaxterAgent::LaunchAnimation, 0.001f, true);
}

void ABaxterAgent::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}

void ABaxterAgent::LaunchAnimation()
{
	ParseAgentData();
}

void ABaxterAgent::ParseAgentData()
{
	std::string AgentData;
	if (!_PendingData.IsEmpty())
	{
		_PendingData.Dequeue(AgentData);

		Document Doc;
		Doc.Parse(AgentData.c_str());
		if (!Doc.HasMember("agent_msg")) return;
		std::string Message = Doc["agent_msg"].GetString();
		
		// Task MazeRL
		if (_ETask == EBaxterAgentTask::TASK_MAZERL && _TaskMazeRL)
		{
			static float* AgentAction = new float[_TaskMazeRL->GetActionDim()];
			bool Reply;

			// 'Reset' execution
			if (_TaskMazeRL->RLMazeResetMsgParser(Message, GetName(), &_TimeStamp, &Reply))
			{
				ABaseAgent* BaseAgentInstance = Cast<ABaseAgent>(this);
				_TaskMazeRL->MazeReset(BaseAgentInstance);

				Doc["agent_msg"].SetString(StringRef(_TaskMsg.c_str()));
				StringBuffer Buf;
				Writer<StringBuffer> Writer(Buf);
				Doc.Accept(Writer);
				std::string ExecutionData = Buf.GetString();

				AVRGymEnvGameMode* CurrentGameMode = (AVRGymEnvGameMode*)GetWorld()->GetAuthGameMode();
				ACentralCommunicator* Comm = CurrentGameMode->GetCentralCommunicator();
				Comm->AgentMessageForward(FString(ExecutionData.c_str()));
			}

			// 'Step' execution
			if (_TaskMazeRL->RLMazeStepMsgParser(Message, GetName(), _TaskMazeRL->GetActionDim(), AgentAction, &_TimeStamp, &Reply))
			{
				ABaseAgent* BaseAgentInstance = Cast<ABaseAgent>(this);
				_TaskMazeRL->MazeStep(BaseAgentInstance, _TaskMazeRL->GetActionDim(), AgentAction);

				Doc["agent_msg"].SetString(StringRef(_TaskMsg.c_str()));
				StringBuffer Buf;
				Writer<StringBuffer> Writer(Buf);
				Doc.Accept(Writer);
				std::string ExecutionData = Buf.GetString();

				AVRGymEnvGameMode* CurrentGameMode = (AVRGymEnvGameMode*)GetWorld()->GetAuthGameMode();
				ACentralCommunicator* Comm = CurrentGameMode->GetCentralCommunicator();
				Comm->AgentMessageForward(FString(ExecutionData.c_str()));
			}
		}

		// Task MotionPlan
		if (_ETask == EBaxterAgentTask::TASK_MOTIONPLAN && _TaskMotionPlan)
		{
			TArray<TTuple<FString, FTransform> > AgentJointsTransforms;

			if (_TaskMotionPlan->MotionPlanMsgParser(Message, GetName(), AgentJointsTransforms))
			{
				ABaseAgent* BaseAgentInstance = Cast<ABaseAgent>(this);
				_TaskMotionPlan->MotionPlanTransform(BaseAgentInstance, _NameToMeshMap, AgentJointsTransforms);

			}
		}
	}
}
