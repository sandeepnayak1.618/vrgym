// Fill out your copyright notice in the Description page of Project Settings.

#include "MessageParser.h"


// Add default functionality here for any IMessageParser functions that are not pure virtual.
//UMessageParser::UMessageParser(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
//{
//}


bool IMessageParser::RLMazeResetMsgParser(std::string Message, FString AgentName, uint64* TimeStamp, bool* Reply)
{
	return false;
}

bool IMessageParser::RLMazeStepMsgParser(std::string Message, FString AgentName, int ActionDim, float* AgentAction, uint64* TimeStamp, bool* Reply)
{
	return false;
}

bool IMessageParser::MotionPlanMsgParser(std::string Message, FString AgentName, TArray<TTuple<FString, FTransform> > &AgentJointsTransforms)
{
	return false;
}