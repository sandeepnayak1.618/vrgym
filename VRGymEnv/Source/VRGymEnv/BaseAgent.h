// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/Actor.h"

#include "AgentMovementComponent.h"
#include <string>

#include "BaseAgent.generated.h"

UCLASS()
class VRGYMENV_API ABaseAgent : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UCameraComponent* _Camera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Baxter Assets")
		UAgentMovementComponent* _MovementComponent;

	// Sets default values for this actor's properties
	ABaseAgent();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void RecvAgentData(std::string AgentData);

	virtual void SetTaskMsg(std::string Msg);

	virtual uint64 GetTimeStamp() const;


protected:
	TQueue<std::string> _PendingData;
	std::string _TaskMsg;
	uint64 _TimeStamp;

	// Agent Animation
	virtual void LaunchAnimation();
	virtual void ParseAgentData();

	// Agent Model
	virtual void InitializeTransMap(UStaticMeshComponent** C, const TCHAR* Name, const TCHAR* MeshPath);
	virtual void InitializeComponent();
};
