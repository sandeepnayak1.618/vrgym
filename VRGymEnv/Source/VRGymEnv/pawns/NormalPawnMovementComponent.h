// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "GameFramework/PawnMovementComponent.h"
#include "NormalPawnMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class VRGYMENV_API UNormalPawnMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()
	
public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
};
