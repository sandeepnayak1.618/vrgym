// Fill out your copyright notice in the Description page of Project Settings.

#include "TouchControllerPawn.h"


// Sets default values
ATouchControllerPawn::ATouchControllerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	SphereComponent->SetupAttachment(SceneComponent);

	RootComponent = SphereComponent;

	BodyMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BodyMesh"));

	SphereComponent->InitSphereRadius(.05f);
	SphereComponent->SetCollisionProfileName(TEXT("Pawn"));

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	PawnMovementComponent = CreateDefaultSubobject<UNormalPawnMovementComponent>(TEXT("NormalPawnMovement"));
	PawnMovementComponent->UpdatedComponent = RootComponent;
}

// Called when the game starts or when spawned
void ATouchControllerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATouchControllerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATouchControllerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("PawnForward", this, &ATouchControllerPawn::PawnForward);
	InputComponent->BindAxis("PawnRight", this, &ATouchControllerPawn::PawnRight);
	InputComponent->BindAxis("PawnTurn", this, &ATouchControllerPawn::PawnTurn);
}

FVector ATouchControllerPawn::ReturnLeftHandLocation()
{
	return LeftHandLocation;
}

FVector ATouchControllerPawn::ReturnRightHandLocation()
{
	return RightHandLocation;
}

FRotator ATouchControllerPawn::ReturnLeftHandRotation()
{
	return LeftHandRotation;
}

FRotator ATouchControllerPawn::ReturnRightHandRotation()
{
	return RightHandRotation;
}

void ATouchControllerPawn::PawnForward(float AxisValue)
{
	if (PawnMovementComponent && PawnMovementComponent->UpdatedComponent == RootComponent)
	{
		PawnMovementComponent->AddInputVector(GetActorForwardVector()*AxisValue);
	}
}

void ATouchControllerPawn::PawnRight(float AxisValue)
{
	if (PawnMovementComponent && PawnMovementComponent->UpdatedComponent == RootComponent)
	{
		PawnMovementComponent->AddInputVector(GetActorRightVector()*AxisValue);
	}
}

void ATouchControllerPawn::PawnTurn(float AxisValue)
{
	FRotator Rot = GetActorRotation();
	Rot.Yaw += AxisValue;
	SetActorRotation(Rot);
}

void ATouchControllerPawn::LeftHandGrabbingIsTrue()
{
	bIsLeftHandGrabbing = true;
}

void ATouchControllerPawn::LeftHandGrabbingIsFalse()
{
	bIsLeftHandGrabbing = false;
}

void ATouchControllerPawn::RightHandGrabbingIsTrue()
{
	bIsRightHandGrabbing = true;
}

void ATouchControllerPawn::RightHandGrabbingIsFalse()
{
	bIsRightHandGrabbing = false;
}

void ATouchControllerPawn::LeftHandPointingIsTrue()
{
	bIsLeftHandPointing = true;
}

void ATouchControllerPawn::LeftHandPointingIsFalse()
{
	bIsLeftHandPointing = false;
}

void ATouchControllerPawn::RightHandPointingIsTrue()
{
	bIsRightHandPointing = true;
}

void ATouchControllerPawn::RightHandPointingIsFalse()
{
	bIsRightHandPointing = false;
}

UPawnMovementComponent* ATouchControllerPawn::GetMovementComponent() const
{
	return PawnMovementComponent;
}