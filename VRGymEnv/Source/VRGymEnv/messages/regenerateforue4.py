# Copyright @ 2016, Code 4 Game, Under The MIT License.

from datetime import *
import os
import sys

comment = '// Added for UE4 in {0}(UTC)'.format(datetime.utcnow())
pragma_line = '#pragma warning (disable : 4125)'

def Check(_CodeFile):
    if (os.path.isfile(_CodeFile) is False):
        raise Exception("Can't find the code file!!!")

    code_file = open(_CodeFile, 'r')
    if (code_file is None):
        raise Exception("Failed to read the code file!!!")
    code_lines = code_file.readlines()
    code_file.close()

    for line in code_lines[:20]:
        if (line.find(pragma_line) < 0):
            continue
        raise Exception("Already was regenerated")
    return True


def Generate(_CodeFile):
    if (Check(_CodeFile) is False):
        raise Exception("Failed to check the code file!!!")

    if (os.path.isfile(_CodeFile) is False):
        raise Exception("Can't find the code file!!!")

    with open(_CodeFile, 'r') as code_file:
        if (code_file is None):
            raise Exception("Failed to read the code file!!!")
        code_lines = code_file.readlines()
        code_file.close()

    with open(_CodeFile, 'w') as code_file:
        if (code_file is None):
            raise Exception("Failed to write the code file!!!")

        meet_include_line = False
        for line in code_lines:
            if (meet_include_line is False and line[0:8] == "#include"):
                code_file.write('\n{0} {1}\n\n'.format(pragma_line, comment))
                meet_include_line = True
            if ("noexcept" in line):
                line = line.replace("noexcept", "")
            code_file.write(line)
        code_file.close()
        if (meet_include_line is False):
            print ("Can't add the pragma warning disable!!!")
        if (meet_include_line):
            print ("Success to regenerate the code for UE4")


if len(sys.argv) <= 1:
    print ("Usage: python regenerateforue4.py `code file`")
else:
    Generate(sys.argv[1])
