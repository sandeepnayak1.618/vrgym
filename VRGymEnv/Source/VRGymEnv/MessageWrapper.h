// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "VRGymEnv.h"
#include "UObject/Interface.h"

#include "messages/transform.pb.h"
#include "messages/rl_maze.pb.h"
#include <string>
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "MessageWrapper.generated.h"

using namespace rapidjson;

// This class does not need to be modified.
UINTERFACE()
class UMessageWrapper : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class VRGYMENV_API IMessageWrapper
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	virtual std::string RLMazeResetMsgWrapper(FString AgentName, std::string State, uint64 TimeStamp);
	virtual std::string RLMazeStepMsgWrapper(FString AgentName, std::string NextState, float Reward, bool Terminal, uint64 TimeStamp);
};
